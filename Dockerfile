FROM python:3.9.7-alpine3.13@sha256:5a7af0085221b26c983b48075ff36d5b7e000220d130cba13d4841a546143b9a

COPY poetry.lock pyproject.toml /

ARG poetry_version=1.1.6
ARG PATH="/root/.poetry/bin:${PATH}"
RUN wget "https://raw.githubusercontent.com/python-poetry/poetry/${poetry_version}/get-poetry.py" \
 && apk add --no-cache -t build-deps \
    cargo \
    gcc \
    libffi-dev \
    musl-dev \
    openssl-dev \
 && python get-poetry.py --version "${poetry_version}" \
 && rm get-poetry.py \
 && poetry config virtualenvs.create false \
 && poetry install --no-root \
 && rm -rf /root/.poetry \
 && apk del build-deps

WORKDIR /code
ENTRYPOINT ["twine"]
